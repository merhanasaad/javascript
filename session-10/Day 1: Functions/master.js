function factorial(number) {
  let result = 1;

  if (number > 0 && number < 11) {
    for (let i = number; i > 0; i--) {
      console.log(result + "*" + i);
      result *= i;
    }
    return result;
  } else {
    const message = console.log("please insert number between 1 to 10");
    return message;
  }
}

const fac = factorial(4);
console.log(fac);
