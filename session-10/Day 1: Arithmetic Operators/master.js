function getArea(height, width) {
  const WidthAndHeightValid = validWidthAndHeight(height, width);
  if (WidthAndHeightValid) {
    const area = height * width;
    return area;
  } else {
    console.log("invalid width and height");
  }
}

function getPerimeter(height, width) {
  const WidthAndHeightValid = validWidthAndHeight(height, width);
  if (WidthAndHeightValid) {
    const perimeter = 2 * (height + width);
    return perimeter;
  } else {
    console.log("invalid width and height");
  }
}

function validWidthAndHeight(height, width) {
  if ((length, width >= 1 && height, width <= 1000)) {
    let rx = /^\d+(?:\.\d{1,2})?$/; //3.55

    if (Number.isInteger(height) && Number.isInteger(width)) {
      return true;
    } else if ((width, height % 1 != 0)) {
      if (rx.test(width) && rx.test(height)) {
        return true;
      }
    }
  } else {
    return false;
  }
}

let width = 3;
let height = 4.55;
let area = getArea(height, width);
let perimeter = getPerimeter(height, width);

console.log(area);
console.log(perimeter);
