import React from 'react';
import ProjectList from "./components/projectList";

class App extends React.Component{
    state = {
        projects: [
            {
                id: "1",
                tittle: "LMS"
            },
            {
                id: "2",
                tittle: "CMS"
            },
            {
                id: "3",
                tittle: "CRN"
            },
            {
                id: "4",
                tittle: "DataBase"
            }
        ],
        newProject: ''
    };

    handleInputValue = e => {
        let newProject = e.target.value;
        this.setState({
            newProject
        });
    };

    addNewBtnClicked = () => {
        let projects = this.state.projects

        projects = [...projects, {
            id: projects.length + 1,
            tittle: this.state.newProject
        }];

        this.setState({
            projects,
            newProject: '' 
        });
    };

    delateBtnClicked = (id) => {
        let projects = this.state.projects.filter(project => project.id !== id);
        this.setState({
            projects
        });
    };

   render(){
        return(
            <div className="container">
                <h1 className="mt-3">Project Management</h1>
                <input type="text" className="form-control mb-2" onChange={this.handleInputValue} value={this.state.newProject}/>
                <button className="btn btn-success mb-3 w-100" onClick={this.addNewBtnClicked}>Add New</button>
                <ProjectList 
                    projects={this.state.projects}
                    onClickDelate={this.delateBtnClicked}
                />
            </div>
        );
   }
};

export default App;
