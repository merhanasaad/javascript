import React from "react";

class projectListItem extends React.Component{
    
    state = {
        project: '',
        editable: false
    };

    editBtnClicked = () => {
        this.setState({
            project: this.props.project.tittle,
            editable: true
        });
    };

    saveBtnClicked = () => {
        const editedProject = this.props.project
        editedProject.tittle = this.state.project

        this.setState({
            project: '',
            editable: false
        });
    }

    handleInputValue = e => {
        const project = e.target.value;
        this.setState({
            project
        });
    };
    
    render(){
       return(
            <li className="list-group-item">
                <a href="#" className="btn">
                    {
                        this.state.editable ? <input  type="text"
                                                      value={this.state.project}
                                                      onChange={this.handleInputValue}/>       
                                            : this.props.project.tittle
                    }
                </a>
                {
                    this.state.editable ? (<button className="btn btn-primary m-2 float-right" onClick={() => this.saveBtnClicked()}>save</button>)
                    : (<button className="btn btn-info m-2 float-right" onClick={() => this.editBtnClicked()}>Edit</button>)
                }
                <button className="btn btn-danger m-2 float-right" onClick={() => this.props.onClickDelate(this.props.project.id)}>Delate</button>
            </li>
       );
    }
};

export default projectListItem;