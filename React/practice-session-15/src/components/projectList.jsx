import React from 'react';
import ProjectListItem from './projectListItem';

const projectList = (props) => (
    <ul className="list-group">
        {props.projects.map(project => (
                <ProjectListItem key={project.id}
                                 project={project}
                                 onClickDelate={props.onClickDelate}/>
            )
        )}
    </ul>
);

export default projectList;