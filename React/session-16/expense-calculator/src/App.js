import React from 'react';

import {
    BrowserRouter,
    Switch,
    Route
}

    from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import NavBar from './layout/navBar';
import ExpensesView from './component/expensesView';
import ListView from './component/listView';
import AddItemView from './component/addItemView';

class App extends React.Component {

    state = {
        items: []
    }

    addBtnClicked = (itemName, itemPrice) => {
        let newItem = { id: this.state.items.length + 1, name: itemName, price: itemPrice }
        let items = this.state.items
        items.push(newItem)
        this.setState({
            itemName
        })
    };

    delateBtnClicked = (delatedItem) => {
        let items = this.state.items
            .filter(item => item !== delatedItem);
        this.setState({
            items
        });
    };

    render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <NavBar />
                    <Switch>
                        <Route exact path="/">
                            <ExpensesView items={this.state.items} calculateTotalPrice={this.calculateTotalPrice} />
                        </Route>
                        <Route exact path="/list">
                            <ListView items={this.state.items} delateBtnClicked={this.delateBtnClicked} />
                        </Route>
                        <Route exact path="/addItem">
                            <AddItemView addBtnClicked={this.addBtnClicked} />
                        </Route>
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}


export default App;