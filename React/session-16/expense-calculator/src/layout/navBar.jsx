import React from 'react';
import { Link, NavLink } from "react-router-dom";

const navBar = () => (
    <nav className="navbar navbar-dark bg-dark">
        <Link className="navbar-brand active" to="/">Expenses</Link>
        <ul className="navbar-nav flex-row mr-auto">
            <li className="nav-item">
                <NavLink className="nav-link ml-2" to="/list">List</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link ml-3" to="/addItem">Add New</NavLink>
            </li>
        </ul>
    </nav>
);

export default navBar;