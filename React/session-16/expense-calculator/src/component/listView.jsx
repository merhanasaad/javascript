import React from 'react';

const listView = (props) => (
    <div className="container text-center">
        {
            (props.items.length) ?
                <div>
                    <h1 className="mt-5 pb-5 text-center border-bottom">Expenses List</h1>
                    <table className="table m-auto">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Item</th>
                                <th scope="col">Price</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                props.items.map(item => (
                                    <tr key={item.id}>
                                        <th scope="row">{item.id}</th>
                                        <td>{item.name}</td>
                                        <td>{Number(item.price)}</td>
                                        <td><button className="btn btn-sm btn-danger" onClick={() => props.delateBtnClicked(item)}>Delate</button></td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
                : <h1 className="m-5">You haven't added any expenses yet</h1>
        }
    </div>
);

export default listView;