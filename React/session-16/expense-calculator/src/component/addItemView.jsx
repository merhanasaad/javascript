import React from 'react';


class addItemView extends React.Component {

    state = {
        itemName: '',
        itemPrice: 0

    }

    handleItemName = e => {
        const itemName = e.target.value
        this.setState({
            itemName
        })
    }

    handleItemPrice = e => {
        const itemPrice = e.target.value
        this.setState({
            itemPrice
        })
    }

    addNewItemHandler = () => {
        let itemName = this.state.itemName;
        let itemPrice = this.state.itemPrice;
        if (itemName === '' || itemPrice === 0) {
            alert("Empty field")
        } else {
            this.props.addBtnClicked(this.state.itemName, this.state.itemPrice)
            this.setState({
                itemName: '',
                itemPrice: 0
            })
        }
    }

    render() {
        return (
            <div className="container">
                <h1 className="mt-5 text-center">Add a new Item</h1>
                <p className="mb-2">Item</p>
                <input type="text" className="form-control mb-3" onChange={this.handleItemName} value={this.state.itemName} />
                <p className="mb-2">Price</p>
                <input type="number" className="form-control mb-3" onChange={this.handleItemPrice} value={this.state.itemPrice} />
                <button className="btn btn-success" onClick={() => this.addNewItemHandler()}>Add Item</button>
            </div >
        );
    }
}

export default addItemView;