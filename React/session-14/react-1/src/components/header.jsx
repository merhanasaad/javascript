import React from 'react'

const header = () => (
   <header className=" bg-main-color">
       <div className="container">
            <nav className="navbar navbar-dark ">
                <a className= "navbar-brand" href="#">Brand</a>
                <ul className="nav">
                    <li className="nav-item"><a class="nav-link w" href="#Home">Home</a></li>
                    <li className="nav-item"><a class="nav-link" href="#About">About</a></li>
                    <li className="nav-item"><a class="nav-link" href="#Contact">Contact</a></li>
                </ul>
            </nav>
        </div>
   </header>
);

export default header;