import React from 'react';

const contactUs = () => (
    <div className="contact-us bg-main-color" id="Contact">
        <div className="container text-center">
            <h3 className="w-100">Contact Us</h3>
            <p className="w-75 mx-auto pb-5">For any inquiries message us</p>
            <div className="row pb-5">
                <div className="col inputfield mr-4">
                    <input type="text" class="form-control mb-5" placeholder="Enter Your Name"/>
                    <input type="email" class="form-control mb-5" id="email" aria-describedby="emailHelp" placeholder="Enter Your Email"/>
                    <input type="text" class="form-control " placeholder="Enter Your Phone Number"/>
                </div>
                <div className="col inputfield">
                    <textarea class="form-control h-100" aria-label="With textarea" placeholder="Enter Your Message"></textarea>
                </div>        
            </div>
            <a href="#Contact"><button class="btn bg-white rounded-pill black">Click Here</button></a>
        </div>
    </div>
);

export default contactUs;