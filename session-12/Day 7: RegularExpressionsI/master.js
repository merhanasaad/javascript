'use strict';

const regexVar = (() => {
    const re = /^([a e o i u]).*\1$/;

    return re;
});

const validString = ((s) => {
    const rx = /^[a-z]+$/;
    if (s.length >= 3 && rx.test(s)) {
        return true;
    } else {
        return false;
    }
});

const rx = regexVar();
const str = "abcdoa";

const test = (validString(str)) ? console.log(rx.test(str)) : console.log("invalid string");