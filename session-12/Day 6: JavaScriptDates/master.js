const getDayName = ((dateString) => {
    const rx = /^(0?[1-9]|1[0-2])\/(0?[1-9]|1\d|2\d|3[01])\/(1[7-9]|2[0-9])\d{2}$/;

    //check date validation
    if (rx.test(dateString)) {
        let date = new Date(dateString);
        const daysName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        //output index 0 : 6 started with sun ... sat
        const dayIndex = date.getDay();
        let dayName = daysName[dayIndex];

        return dayName;
    } else {
        return "invalid date, date range from 1700 : 2999";
    }
});

// MM/DD/YYYY
const date = '12/07/2016';
console.log(getDayName(date));