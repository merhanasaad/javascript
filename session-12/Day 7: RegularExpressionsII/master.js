'use strict';

const regexVar = (() => {
    const re = /^([Mr Mrs Ms Dr Er])+\S+[.][a-zA-Z]+$/;

    return re;
});

const rx = regexVar();
const str = "Er.acsd";

const test = (str.length >= 3) ? console.log(rx.test(str)) : console.log("invalid string");
